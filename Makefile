REBPATH := $(HOME)/dev/seniorproj/rebound
REBXPATH := $(HOME)/dev/seniorproj/reboundx

export OPENGL=1
include $(REBPATH)/src/Makefile.defs

all: librebound.so libreboundx.so
	@echo ""
	@echo "Compiling problem file ..."
	$(CC) -I$(REBPATH)/src/ -I$(REBXPATH)/src/ -Wl,-rpath,./ $(OPT) $(PREDEF) problem.c -L. -lrebound -lreboundx -lm $(LIB) -o rebound
	@echo ""
	@echo "REBOUND compiled successfully."

librebound.so: 
	@echo "Compiling shared library librebound.so ..."
	$(MAKE) -C $(REBPATH)/src/
	@-rm -f librebound.so
	@ln -s $(REBPATH)/src/librebound.so .

libreboundx.so: 
	@echo "Compiling shared library libreboundx.so ..."
	$(MAKE) -C $(REBXPATH)/src/
	@-rm -f libreboundx.so
	@ln -s $(REBXPATH)/src/libreboundx.so .

clean:
	@echo "Cleaning up shared library librebound.so ..."
	@-rm -f librebound.so
	$(MAKE) -C $(REBPATH)/src/ clean
	@echo "Cleaning up shared library libreboundx.so ..."
	@-rm -f libreboundx.so
	$(MAKE) -C $(REBXPATH)/src/ clean
	@echo "Cleaning up local directory ..."
	@-rm -vf rebound
