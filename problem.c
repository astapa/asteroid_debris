#include <stdlib.h>
#include <stdio.h>

#include <math.h>

#include "rebound.h"
#include "reboundx.h"

/* seconds in a day */
#define DAY_SEC (24.0 * 60.0 * 60.0)

/* days in a year */
#define YEAR_DAYS 365.25

/* number of tracers to emit */
#define TRACERS 3000

/* ratio of radiation force to gravity */
#define BETA 10000.0

/* ejection speed, in km/day (currently 1 m/s) */
#define EJECT_SPEED 0.0 /*86.4*/

/* how long to emit them for */
#define EMISSION_PERIOD (10.0 * YEAR_DAYS)
#define EJECT_PERIOD (EMISSION_PERIOD / (TRACERS - 1))

#define SIMULATION_PERIOD (100.0 * YEAR_DAYS)

/* asteroid parameters; this is Bennu */
double asteroid_pos[3] = {1.074238138393520E+08, -1.428968794327393E+08, -1.550854652433877E+07};
double asteroid_vel[3] = {1.508128652252610E+06, 1.668592678195782E+06, 1.708735175096218E+05};
double asteroid_mass = 7.3e10;

/* gravitational constant, using days for time and km for distance */
#define G_CONST (6.67430e-11 * DAY_SEC * DAY_SEC * 1.0e-9)

/* speed of light, in km/day */
#define C_CONST (3.0e5 * DAY_SEC)

/* THE STANDARD */
#define TAU 6.283185307

/* initial positions of major bodies, relative to system barycenter,
 * in km
 */
double major_positions[10][3] = 
{
    {-1.327155365856181E+06, 3.696435592165196E+05, 2.797429692706882E+04}, // sun
    {1.334496507271761E+07, 4.395683829943987E+07, 2.244044995240673E+06}, // mercury
    {-2.035027308396209E+07, -1.066131883528975E+08, -3.429456862017587E+05}, // venus
    {-1.396280999072726E+08, -5.762980575443690E+07, 3.120776457355171E+04}, // earth
    {8.047184777280146E+07, -1.946809809379555E+08, -6.066378497280762E+06}, // mars
    {7.267660665202591E+08, -1.522532746374396E+08, -1.562792499377201E+07}, // jupiter
    {1.094160137024642E+09, -9.954688038664324E+08, -2.625450639283758E+07}, // saturn
    {2.110665218390694E+09, 2.057479088704634E+09, -1.970253045827866E+07}, // uranus
    {4.437764022619999E+09, -5.636795747701788E+08, -9.066475894411668E+07}, // neptune
    {-1.399788696294122E+08, -5.746596747781532E+07, 6.569382369634882E+04}, // moon

};

/* initial velocities of major bodies, relative to system barycenter,
 * in km/day
 */
double major_vels[10][3] = 
{
    {-3.502052164977099E+02, -1.314794582172842E+03, 1.878310709667860E+01}, // sun
    {-4.832176301761578E+06, 1.501369879214307E+06, 5.660246067399939E+05}, // mercury
    {2.958332860666159E+06, -5.430327423608952E+05, -1.781411544773098E+05}, // venus
    {9.535026673643695E+05, -2.383647606686622E+06, 1.590354222323754E+02}, // earth
    {2.009023935548482E+06, 9.882406673502700E+05, -2.853202911738330E+04}, // mars
    {2.180096879554157E+05, 1.157683871345308E+06, -9.688687371699123E+03}, // jupiter
    {5.148077476262210E+05, 6.156783466060382E+05, -3.121094202635951E+04}, // saturn
    {-4.150464152956381E+05, 3.939230634997796E+05, 6.859223481345047E+03}, // uranus
    {5.604955713454526E+04, 4.686894664332189E+05, -1.089620451488410E+04}, // neptune
    {9.211822655052242E+05, -2.464807553846208E+06, -1.997288671074790E+03}, // moon
};

/* masses of major bodies, in kg */
double major_masses[10] =
{
    1.9885e30, // sun
    3.302e23, // mercury
    4.8685e24, // venus
    5.97219e24, // earth
    6.4171e23, // mars
    1.89818722e27, // jupiter
    5.6834e26, // saturn
    8.6813e25, // uranus
    1.02409e26, // neptune
    7.349e+22, // moon
};

/* calculates ejection velocity for a particle ejected from body moving
 * with velocity vel, ejected with relative speed spd
 * in a random direction
 */
void random_eject(struct reb_simulation *r, struct rebx_extras *rebx, struct reb_particle from, double spd);

void heartbeat(struct reb_simulation *r);

static struct rebx_extras *rebx;

int main(int argc, char* argv[]){
	/*struct reb_simulation *r = reb_create_simulation();
    r->dt = EJECT_PERIOD;
    r->G = G_CONST;
    r->heartbeat = heartbeat;
    r->exact_finish_time = 1;
    r->integrator = REB_INTEGRATOR_IAS15;
    r->usleep = 10000.0;
    reb_add_fmt(r, "m", major_masses[0]);
    struct reb_particle a = {0};
    a.x = asteroid_pos[0];
    a.y = asteroid_pos[1];
    a.z = asteroid_pos[2];
    a.vx = asteroid_vel[0];
    a.vy = asteroid_vel[1];
    a.vz = asteroid_vel[2];
    a.m = 0.0;
    reb_add(r, a);
    rebx = rebx_attach(r);
    struct rebx_force *rad = rebx_load_force(rebx, "radiation_forces");
    rebx_set_param_double(rebx, &rad->ap, "c", C_CONST);
    
    reb_add(r, a);
    rebx_set_param_double(rebx, &r->particles[2].ap, "beta", BETA);

    reb_move_to_com(r);
    reb_integrate(r, SIMULATION_PERIOD);
    printf("\n%e\n%e\n", r->particles[1].x, r->particles[2].x);
    rebx_free(rebx);
    reb_free_simulation(r);*/

    struct reb_simulation* r = reb_create_simulation();
    r->dt = EJECT_PERIOD;
    r->G = G_CONST;
    r->heartbeat = heartbeat;
    r->exact_finish_time = 1; // finish when asked to
    r->integrator = REB_INTEGRATOR_IAS15; // adaptive timestep, high resolution
    r->N_active = 10; // solar system - asteroid
    
    r->usleep = 1000.0; // slow the simulation down a lot

	int i;

    // create the solar system
    for (i = 0; i < 10; ++i){
        struct reb_particle p = {0};
        p.x  = major_positions[i][0];
        p.y  = major_positions[i][1];
        p.z  = major_positions[i][2];
        
        p.vx = major_vels[i][0];
        p.vy = major_vels[i][1];
        p.vz = major_vels[i][2];
        
        p.m  = major_masses[i];
        reb_add(r, p); 
    }
    
    // add the asteroid
    struct reb_particle a = {0};
    a.x = asteroid_pos[0];
    a.y = asteroid_pos[1];
    a.z = asteroid_pos[2];
    a.vx = asteroid_vel[0];
    a.vy = asteroid_vel[1];
    a.vz = asteroid_vel[2];
    a.m = 0.0;
    reb_add(r, a);
    
    // reboundx setup
    rebx = rebx_attach(r);
    struct rebx_force *rad = rebx_load_force(rebx, "radiation_forces");
    rebx_set_param_double(rebx, &rad->ap, "c", C_CONST);
    rebx_set_param_int(rebx, &r->particles[0].ap, "radiation_source", 1);
    
    // eject the first tracer
    random_eject(r, rebx, r->particles[10], EJECT_SPEED);
    
    // simulate
    reb_move_to_com(r);
    reb_integrate(r, INFINITY);//SIMULATION_PERIOD);
    rebx_free(rebx);
    reb_free_simulation(r);
}

void random_eject(struct reb_simulation *r, struct rebx_extras *rebx, struct reb_particle from, double spd)
{
	/* pick a random point on the sphere
	 * thanks stackexchange :D
	 * https://math.stackexchange.com/questions/44689/how-to-find-a-random-axis-or-unit-vector-in-3d
	 */
	double u, v, z, theta, x, y;
	
	/* thanks stackoverflow too :)
	 * https://stackoverflow.com/questions/13408990/how-to-generate-random-float-number-in-c
	 */
	u = (double)rand() / (double)RAND_MAX; // not great, but you could do worse
	v = (double)rand() / (double)RAND_MAX;
	z = (v * 2.0) - 1.0; // range shift
	theta = TAU * u;
	x = sqrt(1 - (z * z)) * cos(theta);
	y = sqrt(1 - (z * z)) * sin(theta);
	
	// eject!
	struct reb_particle p = {0};
	p.x = from.x;
	p.y = from.y;
	p.z = from.z;
	p.vx = from.vx + (x * spd);
	p.vy = from.vy + (y * spd);
	p.vz = from.vz + (z * spd);
    p.m = 0.0;
    
	reb_add(r, p);
    rebx_set_param_double(rebx, &r->particles[r->N - 1].ap, "beta", BETA);
}

void heartbeat(struct reb_simulation *r)
{
	if (r->t == 0.0)
	{
		// a blocking call to let me take some pretty pictures
		getc(stdin);
	}
	/*if ((r->N - 10) < (TRACERS + 1))
	{
		reb_integrator_synchronize(r);
		r->dt = EJECT_PERIOD;
		random_eject(r, rebx, r->particles[10], EJECT_SPEED);
	}*/
	
	if (reb_output_check(r, YEAR_DAYS)){
        reb_output_timing(r, SIMULATION_PERIOD);
    }
}
